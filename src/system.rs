use std::process::Command;

/// Causes system to hibernate.
///
/// The method uses `systemctl` in order to start hibernation.
pub fn hibernate() {
    Command::new("systemctl")
        .arg("hibernate")
        .spawn()
        .ok();
}
