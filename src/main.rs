extern crate batteryd;
extern crate env_logger;

use batteryd::battery::Battery;
use batteryd::events::BatteryEventPublisher;
use batteryd::notifier::Notifier;

fn main() {
    env_logger::init();

    BatteryEventPublisher::new(&Battery::new(), &Notifier::new()).run();
}
