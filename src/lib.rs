#![warn(missing_docs)]

#[macro_use]
extern crate log;

pub mod battery;
pub mod estimation;
pub mod events;
pub mod notifications;
pub mod notifier;
pub mod system;

/// Return the version number of the library.
pub fn version() -> String {
    env!("CARGO_PKG_VERSION").to_string()
}
