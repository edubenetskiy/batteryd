extern crate notify_rust;

use self::notify_rust::{Error, Notification, NotificationHandle, NotificationUrgency};
use std::fmt;

#[derive(Debug, Copy, Clone)]
pub enum Urgency {
    Low,
    Normal,
    Critical,
}

impl fmt::Display for Urgency {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl From<Urgency> for NotificationUrgency {
    fn from(urgency: Urgency) -> Self {
        match urgency {
            Urgency::Low => NotificationUrgency::Low,
            Urgency::Normal => NotificationUrgency::Normal,
            Urgency::Critical => NotificationUrgency::Critical,
        }
    }
}

pub struct NotificationOptions {
    title: String,
    text: String,
    icon: String,
    expire_time: Option<u32>,
    urgency: Urgency,
}

impl NotificationOptions {
    pub fn new() -> Self {
        NotificationOptions {
            title: String::new(),
            text: String::new(),
            icon: String::new(),
            expire_time: None,
            urgency: Urgency::Normal,
        }
    }

    pub fn title<S: ToString>(&mut self, title: S) -> &mut Self {
        self.title = title.to_string();
        self
    }

    pub fn text<S: ToString>(&mut self, text: S) -> &mut Self {
        self.text = text.to_string();
        self
    }

    pub fn icon<S: ToString>(&mut self, icon: S) -> &mut Self {
        self.icon = icon.to_string();
        self
    }

    pub fn urgency(&mut self, urgency: Urgency) -> &mut Self {
        self.urgency = urgency;
        self
    }

    pub fn expire_time(&mut self, milliseconds: u32) -> &mut Self {
        self.expire_time = Some(milliseconds);
        self
    }

    pub fn eternal(&mut self) -> &mut Self {
        self.expire_time(0)
    }

    pub fn show(&self) -> Result<NotificationHandle, Error> {
        Notification::new()
            .summary(&self.title)
            .body(&self.text)
            .icon(&self.icon)
            .appname(&self.icon) // FIXME
            .timeout(self.expire_time.map(|i| i as i32).unwrap_or(-1))
            .urgency(NotificationUrgency::from(self.urgency))
            .show()
    }
}
