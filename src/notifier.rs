extern crate notify_rust;

use battery::Battery;
use events::BatteryEventSubscriber;
use notifications::{NotificationOptions, Urgency};
use self::notify_rust::{Error, NotificationHandle};
use std::thread;
use std::time::Duration;
use system;

pub struct Notifier {}

impl Notifier {
    pub fn new() -> Self {
        Notifier {}
    }

    /// Show a desktop notification
    fn notify(&self,
              title: &str,
              text: &str,
              icon: &str,
              urgency: Urgency)
              -> Result<NotificationHandle, Error> {
        NotificationOptions::new()
            .title(title)
            .text(text)
            .icon(icon)
            .urgency(urgency)
            .show()
    }
}

impl BatteryEventSubscriber for Notifier {
    // TODO: Choose an icon corresponding to the current battery level
    fn battery_charging(&self, battery: &Battery) {
        NotificationOptions::new()
            .title("Батарея заряжается")
            .text(match battery.capacity() {
                Ok(capacity) => {
                    format!("Уровень заряда батареи: {}%.", capacity)
                }
                _ => String::new(),
            })
            .icon(match battery.capacity() {
                Ok(x) if x <= 5 => "battery-caution-charging",
                Ok(x) if x <= 50 => "battery-low-charging",
                Ok(x) if x <= 95 => "battery-good-charging",
                _ => "battery-full-charging",
            })
            .show()
            .ok();
    }

    fn battery_full(&self, _battery: &Battery) {
        NotificationOptions::new()
            .title("Батарея заряжена")
            .text("Отключите зарядное устройство от ноутбука.")
            .icon("battery-full-charged")
            .eternal()
            .show()
            .ok();
    }

    fn battery_discharging(&self, battery: &Battery) {
        NotificationOptions::new()
            .title("Батарея разряжается")
            .text(match battery.capacity() {
                Ok(capacity) => {
                    format!("Уровень заряда батареи: {}%.", capacity)
                }
                _ => String::new(),
            })
            .icon(match battery.capacity() {
                Ok(x) if x <= 5 => "battery-caution",
                Ok(x) if x <= 50 => "battery-low",
                Ok(x) if x <= 95 => "battery-good",
                _ => "battery-full",
            })
            .show()
            .ok();
    }

    fn battery_low(&self, battery: &Battery) {
        NotificationOptions::new()
            .title("Батарея разряжена")
            .text(match battery.capacity() {
                Ok(capacity) => format!("Уровень заряда батареи: {}%. <b>Подключите зарядное устройство.</b>", capacity),
                Err(_) => String::from("<b>Подключите зарядное устройство.</b>"),
            })
            .icon("battery-low")
            .eternal()
            .show().ok();
    }

    fn battery_critical(&self, battery: &Battery) {
        NotificationOptions::new()
            .title("Батарея полностью разряжена")
            .text("Через минуту компьютер будет переведён в спящий режим. \
                   <b>Подключите зарядное устройство.</b>")
            .icon("battery-caution")
            .urgency(Urgency::Critical)
            .eternal()
            .show().ok();

        info!("Battery critical, hibernating in 1 minute");

        for _ in 0..12 {
            thread::sleep(Duration::from_secs(5));
            if battery.is_charging() {
                info!("Hibernation cancelled");
                NotificationOptions::new()
                    .title("Гибернация отменена")
                    .icon("weather-clear")
                    .show()
                    .ok();
                return;
            }
        }

        NotificationOptions::new()
            .title("Батарея полностью разряжена")
            .text("Компьютер будет немедленно переведён в спящий режим.")
            .icon("weather-clear-night")
            .urgency(Urgency::Critical)
            .show().ok();

        info!("Hibernating");
        system::hibernate();
    }

    fn battery_missing(&self, _battery: &Battery) {
        self.notify("Батарея отключена",
                    "Не отключайте компьютер от сети электропитания.",
                    "battery-missing",
                    Urgency::Normal).ok();
    }
}
