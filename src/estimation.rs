use battery::Battery;

pub struct BatteryEstimator<'b> {
    battery: &'b Battery,
}

// TODO: Implement BatteryEstimator
// This is going to be useful for distinguishing battery levels
// such as Critical, Low, Medium, High, Full.

impl<'b> BatteryEstimator<'b> {
    pub fn new(battery: &'b Battery) -> Self {
        BatteryEstimator { battery }
    }
}
