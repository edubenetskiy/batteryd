use battery::{Battery, Status};
use std::cell::Cell;
use std::thread;
use std::time;

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum Level {
    Normal,
    Critical,
    Low,
    Full,
}

pub struct BatteryEventPublisher<'b, 'r, T: 'r + BatteryEventSubscriber> {
    battery: &'b Battery,
    receiver: &'r T,

    last_status: Cell<Status>,
    last_capacity: Cell<Result<u8, ()>>,
    last_level: Cell<Level>,
}

pub trait BatteryEventSubscriber {
    fn battery_charging(&self, battery: &Battery) {}
    fn battery_full(&self, battery: &Battery) {}
    fn battery_discharging(&self, battery: &Battery) {}
    fn battery_low(&self, battery: &Battery) {}
    fn battery_critical(&self, battery: &Battery) {}
    fn battery_missing(&self, battery: &Battery) {}
}

impl<'b, 'r, T> BatteryEventPublisher<'b, 'r, T>
    where T: 'r + BatteryEventSubscriber
{
    pub fn new(battery: &'b Battery, receiver: &'r T) -> BatteryEventPublisher<'b, 'r, T> {
        BatteryEventPublisher {
            battery,
            receiver,
            last_status: Cell::new(battery.status()),
            last_capacity: Cell::new(battery.capacity()),
            last_level: Cell::new(Level::Normal),
        }
    }

    pub fn run(&self) {
        // FIXME: Use logger instead of `println!` macro
        info!("Event publisher up and running.");
        loop {
            let capacity = self.battery.capacity();
            let status = self.battery.status();
            let level = match capacity {
                Ok(cap) => {
                    match status {
                        Status::Discharging if cap <= 5 => Level::Critical,
                        Status::Discharging if cap <= 15 => Level::Low,
                        Status::Full => Level::Full,
                        _ => Level::Normal,
                    }
                }
                Err(_) => Level::Normal,
            };

            if (self.last_capacity.get(), self.last_level.get(), self.last_status.get()) !=
                (capacity, level, status) {
                debug!("Current battery status is {}, capacity is {:?}, level is {:?}",
                       status,
                       capacity,
                       level);
            }

            match (self.last_status.get(), status) {
                // TODO: Process Status::Missing in both left and right-hand expressions
                (last, new) if last == new => {}
                (_, Status::Full) => self.receiver.battery_full(&self.battery),
                (_, Status::Charging) => self.receiver.battery_charging(&self.battery),
                (_, Status::Discharging) => self.receiver.battery_discharging(&self.battery),
                (_, Status::Missing) => self.receiver.battery_missing(&self.battery),
            };

            match (self.last_level.get(), level) {
                (last, new) if last == new => {}
                (Level::Critical, Level::Low) => {}

                (_, Level::Low) => self.receiver.battery_low(&self.battery),
                (_, Level::Critical) => self.receiver.battery_critical(&self.battery),

                _ => {
                    debug!("Unprocessed level change from {:?} to {:?}",
                           self.last_level.get(),
                           level);
                }
            };

            self.last_status.set(status);
            self.last_capacity.set(capacity);
            self.last_level.set(level);

            thread::sleep(time::Duration::from_millis(1_000));
        }
    }
}
