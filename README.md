# batteryd

A lightweight battery event notification daemon for Linux.

## Features

- Hibernate on low battery
- Desktop notifications:
    - AC plug on/off
    - Battery low/full

## Requirements

- `systemd` for hibernation
- `acpi` for getting battery status
- Some desktop notification daemon (usually shipped with your DM)

## License

Batteryd is distributed under the terms of the MIT license.

See LICENSE for details.

